import React from 'react'
import {Fragment, useEffect, useState} from 'react'
import { Container, Row, Col } from 'react-grid-system';
import ProductCard from '../components/ProductCardBrowse'
import "../App.css";

const ProductsPage = () => {
	const[products, setProducts] = useState([])
	useEffect(() => {
		fetch(`${process.env.REACT_APP_BACKEND_URL}/products/all`)
			.then(res => res.json())
			.then(data => {
				setProducts(data.map(product => {
					return (
						<ProductCard key = {product.id} productProp = {product} />
					)
				}))
			})
	}, [])

	return (
		<Fragment>				
			<Container style={{marginTop: "20px", marginBottom: "20px"}}>
				<Row style={{marginTop: "20px", marginBottom: "20px"}}>
				{ products.map(item => (
					<Col sm={4}>{item}</Col>
					))}											
				</Row>
			</Container>
		</Fragment>
	)
}

export default ProductsPage