import React from 'react'
import {Fragment} from 'react'
import ReactImageMagnify from 'react-image-magnify'

const ProductCard = (prop) => {
	return (
		<Fragment>		
			<div style = {{width: '400px'}}>
				<ReactImageMagnify {...{
				    smallImage: {
				        alt: 'Wristwatch by Ted Baker London',
				        isFluidWidth: true,
				        src: `${process.env.REACT_APP_BACKEND_URL}/${prop.image1}`
				    },
				    largeImage: {
				        src: `${process.env.REACT_APP_BACKEND_URL}/${prop.image2}`,
				        width: 1600,
				        height: 1600,
				    },
				    enlargedImagePosition: 'over'
				}} />
			</div>
		</Fragment>
	)
}

export default ProductCard